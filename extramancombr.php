<?php
/**
 * Extra Man - mu-plugin
 *
 * PHP version 7
 *
 * @category  Wordpress_Mu-plugin
 * @package   Extra Man
 * @author    Agência Colla <contato@agenciacolla.com.br>
 * @copyright 2022 Agência Colla
 * @license   Proprietary https://agenciacolla.com.br
 * @link      https://agenciacolla.com.br
 *
 * @wordpress-plugin
 * Plugin Name: Extra Man - mu-plugin
 * Plugin URI:  https://agenciacolla.com.br
 * Description: Customizations for extraman.com.br site
 * Version:     1.0.0
 * Author:      Agência Colla
 * Author URI:  https://agenciacolla.com.br/
 * Text Domain: extramancombr
 * License:     Proprietary
 * License URI: https://agenciacolla.com.br
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Load Translation
 *
 * @return void
 */
add_action(
    'plugins_loaded',
    function () {
        load_muplugin_textdomain('extramancombr', basename(dirname(__FILE__)).'/languages');
    }
);

/**
 * Hide editor
 */

add_action(
    'admin_head',
    function () {
        $template_file = $template_file = basename( get_page_template() );

        $templates = array("page-politica-de-privacidade.blade.php");
        if (!in_array($template_file, $templates)) { 
            remove_post_type_support('page', 'editor');
        }
    }
);

/***********************************************************************************
 * Callback Functions
 **********************************************************************************/

/**********
 * Show on Front Page
 *
 * @return bool $display
 */
function Show_On_Front_page($cmb)
{
    // Get ID of page set as front page, 0 if there isn't one
    $front_page = get_option('page_on_front');

    // Get the translated page of the curent language
    $translated_page = function_exists('pll_get_post') ? pll_get_post($front_page) : $front_page;

    // There is a front page set  - are we editing it?
    return $cmb->object_id() == $translated_page;
}

/**********
 * Show on Privacy Policy 
 *
 * @return bool $display
 */
function Show_On_Privacy($cmb)
{
    // Get ID of page set as privacy, 0 if there isn't one
    $privacy = get_option('wp_page_for_privacy_policy');

     // Get the translated page of the curent language
    $translated_page = function_exists('pll_get_post') ? pll_get_post($privacy) : $privacy;

    // There is a privacy set  - are we editing it?
    return $cmb->object_id() == $translated_page;
}

/***********************************************************************************
 * Page Fields
 * ********************************************************************************/

/**
 * Front-page
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_extraman_frontpage_';

        
        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => '_extramancombr_frontpage_hero_id',
                'title'         => __('Hero', 'extramancombr'),
                'show_names'    => true,
                'object_types'  => array('page'), // post type
                'show_on_cb'    => 'Show_On_Front_page',
                'context'       => 'normal',
                'priority'      => 'high',
            )
        );

        //Background Color
        $cmb_hero->add_field(
            array(
                'name'       => __('Background Color', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'hero_bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#fbbf24', '#f97316', '#00E6FF', '#002A54', '#001334'),
                        ),
                    ),
                ),
            ) 
        );

        //Hero Background Image (Desktop)
        $cmb_hero->add_field(
            array(
                'name'        => __('Background Image (Desktop)', 'extramancombr'),
                'desc'        => __('Format: JPG (recommended), PNG | Recommended size: 2560x700px (Content centered at 1920px)', 'extramancombr'),
                'id'          => $prefix . 'hero_bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'extramancombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 77)
            )
        );

        //Hero Background Image (Mobile)
        $cmb_hero->add_field(
            array(
                'name'        => __('Background Image (Mobile)', 'extramancombr'),
                'desc'        => __('Format: JPG (recommended), PNG | Recommended size: 1024x1280px', 'extramancombr'),
                'id'          => $prefix . 'hero_bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'extramancombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 350)
            )
        );
        
        //Hero Button Text
        $cmb_hero->add_field(
            array(
                'name'       => __('Button Text', 'extramancombr'),
                'desc'       => '',
                'default'    => __('Buy now', 'extramancombr'),
                'id'         => $prefix . 'hero_btn_text',
                'type'       => 'text_medium',
            )
        );

        //Hero Button URL
        $cmb_hero->add_field(
            array(
                'name'       => __('Button URL', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'hero_btn_url',
                'type'       => 'textarea_code',
                'attributes' => array (
                    'rows'  => 2,
                ),
            )
        );
        
        /**
        * Video
        */
        $cmb_video = new_cmb2_box(
            array(
                'id'            => '_extraman_frontpage_video_id',
                'title'         => __('Video', 'extramancombr'),
                'object_types'  => array('page'), // post type
                'show_on_cb'    => 'Show_On_Front_page',
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
            )
        );

        //Title
        $cmb_video->add_field(
            array(
                'name'       => __('Title', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'video_title',
                'type'       => 'text',
            )
        );

        //Subtitle
        $cmb_video->add_field(
            array(
                'name'       => __('Subtitle', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'video_subtitle',
                'type'       => 'text',
            )
        );

        //Video URL
        $cmb_video->add_field( 
            array(
                'name'       => __('Video (URL)', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'video_url',
                'type'       => 'text',
            )
        );

        //Button Text
        $cmb_video->add_field(
            array(
                'name'       => __('Button Text', 'extramancombr'),
                'desc'       => '',
                'default'    => __('Buy now', 'extramancombr'),
                'id'         => $prefix . 'video_btn_text',
                'type'       => 'text_medium',
            )
        );

        //Button URL
        $cmb_video->add_field(
            array(
                'name'       => __('Button URL', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'video_btn_url',
                'type'       => 'textarea_code',
                'attributes' => array (
                    'rows'  => 2,
                ),
            )
        );
        
        //Seals Group
        $seals_id = $cmb_video->add_field(
            array(
                'id'          => $prefix . 'video_seals',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   => __('Seal {#}', 'extramancombr'),
                    'add_button'    => __('Add Another Seal', 'extramancombr'),
                    'remove_button' => __('Remove Seal', 'extramancombr'),
                    'sortable'      => true,
                ),
            )
        );

        //Seal Icon
        $cmb_video->add_group_field(
            $seals_id,
            array(
                'name'    => __('Seal Icon', 'extramancombr'),
                'desc'    => __('Format: SVG (recommended), PNG | Recommended size: 320x320 | Aspect ratio: 1:1', 'extramancombr'),
                'id'      => 'seal_icon',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Icon', 'extramancombr'), 
                ),
                'query_args' => array(
                    'type' => array(
                        //'image/jpeg',
                        'image/png',
                        'image/svg+xml',
                    ),
                ),
                'preview_size' => array(64, 64) 
            ) 
        );

        /**
        * Info Section 1
        */
        $cmb_infosec1 = new_cmb2_box(
            array(
                'id'            => '_extraman_frontpage_infosec1_id',
                'title'         => __('Info Section 1', 'extramancombr'),
                'object_types'  => array('page'), // post type
                'show_on_cb'    => 'Show_On_Front_page',
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
            )
        );

        //Background Color
        $cmb_infosec1->add_field(
            array(
                'name'       => __('Background Color', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'infosec1_bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#fbbf24', '#f97316', '#00E6FF', '#002A54', '#001334'),
                        ),
                    ),
                ),
            ) 
        );

        //Background Image (Desktop)
        $cmb_infosec1->add_field(
            array(
                'name'        => __('Background Image (Desktop)', 'extramancombr'),
                'desc'        => __('Format: JPG (recommended), PNG | Recommended size: 2560x1080px (Content centered at 1920px)', 'extramancombr'),
                'id'          => $prefix . 'infosec1_bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'extramancombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 77)
            )
        );

        //Background Image (Mobile)
        $cmb_infosec1->add_field(
            array(
                'name'        => __('Background Image (Mobile)', 'extramancombr'),
                'desc'        => __('Format: JPG (recommended), PNG | Recommended size: 1024x1280px', 'extramancombr'),
                'id'          => $prefix . 'infosec1_bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'extramancombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 350)
            )
        );

        //Title
        $cmb_infosec1->add_field(
            array(
                'name'       => __('Title', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'infosec1_title',
                'type'       => 'textarea_code',
            )
        );

        //Subtitle
        $cmb_infosec1->add_field(
            array(
                'name'       => __('Subtitle', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'infosec1_subtitle',
                'type'       => 'textarea_code',
            )
        );

        //Content
        $cmb_infosec1->add_field(
            array(
                'name'       => __('Content', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'infosec1_content',
                'type'       => 'textarea_code',
            )
        );

        //Button Text
        $cmb_infosec1->add_field(
            array(
                'name'       => __('Button Text', 'extramancombr'),
                'desc'       => '',
                'default'    => __('Buy now', 'extramancombr'),
                'id'         => $prefix . 'infosec1_btn_text',
                'type'       => 'text_medium',
            )
        );

        //Button URL
        $cmb_infosec1->add_field(
            array(
                'name'       => __('Button URL', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'infosec1_btn_url',
                'type'       => 'textarea_code',
                'attributes' => array (
                    'rows'  => 2,
                ),
            )
        );

        /**
        * Info Section 2
        */
        $cmb_infosec2 = new_cmb2_box(
            array(
                'id'            => '_extraman_frontpage_infosec2_id',
                'title'         => __('Info Section 2', 'extramancombr'),
                'object_types'  => array('page'), // post type
                'show_on_cb'    => 'Show_On_Front_page',
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
            )
        );

        //Background Color
        $cmb_infosec2->add_field(
            array(
                'name'       => __('Background Color', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'infosec2_bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#fbbf24', '#f97316', '#00E6FF', '#002A54', '#001334'),
                        ),
                    ),
                ),
            ) 
        );

        //Background Image (Desktop)
        $cmb_infosec2->add_field(
            array(
                'name'        => __('Background Image (Desktop)', 'extramancombr'),
                'desc'        => __('Format: JPG (recommended), PNG | Recommended size: 2560x1080px (Content centered at 1920px)', 'extramancombr'),
                'id'          => $prefix . 'infosec2_bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'extramancombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 77)
            )
        );

        //Background Image (Mobile)
        $cmb_infosec2->add_field(
            array(
                'name'        => __('Background Image (Mobile)', 'extramancombr'),
                'desc'        => __('Format: JPG (recommended), PNG | Recommended size: 1024x1280px', 'extramancombr'),
                'id'          => $prefix . 'infosec2_bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'extramancombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 350)
            )
        );

        //Title
        $cmb_infosec2->add_field(
            array(
                'name'       => __('Title', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'infosec2_title',
                'type'       => 'textarea_code',
            )
        );

        //Subtitle
        $cmb_infosec2->add_field(
            array(
                'name'       => __('Subtitle', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'infosec2_subtitle',
                'type'       => 'textarea_code',
            )
        );

        //Content
        $cmb_infosec2->add_field(
            array(
                'name'       => __('Content', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'infosec2_content',
                'type'       => 'textarea_code',
            )
        );

        //Button Text
        $cmb_infosec2->add_field(
            array(
                'name'       => __('Button Text', 'extramancombr'),
                'desc'       => '',
                'default'    => __('Buy now', 'extramancombr'),
                'id'         => $prefix . 'infosec2_btn_text',
                'type'       => 'text_medium',
            )
        );

        //Button URL
        $cmb_infosec2->add_field(
            array(
                'name'       => __('Button URL', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'infosec2_btn_url',
                'type'       => 'textarea_code',
                'attributes' => array (
                    'rows'  => 2,
                ),
            )
        );

        /**
        * Benefits
        */
        $cmb_benefits = new_cmb2_box(
            array(
                'id'            => '_extraman_frontpage_benefits_id',
                'title'         => __('Benefits', 'extramancombr'),
                'object_types'  => array('page'), // post type
                'show_on_cb'    => 'Show_On_Front_page',
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
            )
        );

        //Banner 1 (Desktop)
        $cmb_benefits->add_field(
            array(
                'name'        => __('Banner 1 (Desktop)', 'extramancombr'),
                'desc'        => __('Format: JPG (recommended), PNG | Recommended size: 1920x450px', 'extramancombr'),
                'id'          => $prefix . 'benefits_banner1_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'extramancombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 77)
            )
        );

        //Banner 1 (Mobile)
        $cmb_benefits->add_field(
            array(
                'name'        => __('Banner 1 (Mobile)', 'extramancombr'),
                'desc'        => __('Format: JPG (recommended), PNG | Recommended size: 1024x1280px', 'extramancombr'),
                'id'          => $prefix . 'benefits_banner1_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'extramancombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 350)
            )
        );

        //Benefits Group
        $benefits_id = $cmb_benefits->add_field(
            array(
                'id'          => $prefix . 'benefits_benefits',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   => __('Benefit {#}', 'extramancombr'),
                    'add_button'    => __('Add Another Benefit', 'extramancombr'),
                    'remove_button' => __('Remove Benefit', 'extramancombr'),
                    'sortable'      => true,
                ),
            )
        );

        //Benefit Image
        $cmb_benefits->add_group_field(
            $benefits_id,
            array(
                'name'        => __('Benefit Image', 'extramancombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1080x1080px', 'extramancombr'),
                'id'          => 'img',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'extramancombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 280)
            )
        );

        //Benefit Title
        $cmb_benefits->add_group_field(
            $benefits_id,
            array(
                'name'       => __('Benefit Title', 'extramancombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'text',
            )
        );

        //Button Text
        $cmb_benefits->add_field(
            array(
                'name'       => __('Button Text', 'extramancombr'),
                'desc'       => '',
                'default'    => __('Buy now', 'extramancombr'),
                'id'         => $prefix . 'benefits_btn_text',
                'type'       => 'text_medium',
            )
        );

        //Button URL
        $cmb_benefits->add_field(
            array(
                'name'       => __('Button URL', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'benefits_btn_url',
                'type'       => 'textarea_code',
                'attributes' => array (
                    'rows'  => 2,
                ),
            )
        );

        //Banner 2 (Desktop)
        $cmb_benefits->add_field(
            array(
                'name'        => __('Banner 2 (Desktop)', 'extramancombr'),
                'desc'        => __('Format: JPG (recommended), PNG | Recommended size: 1920x450px', 'extramancombr'),
                'id'          => $prefix . 'benefits_banner2_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'extramancombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 77)
            )
        );

        //Banner 2 (Mobile)
        $cmb_benefits->add_field(
            array(
                'name'        => __('Banner 2 (Mobile)', 'extramancombr'),
                'desc'        => __('Format: JPG (recommended), PNG | Recommended size: 1024x1280px', 'extramancombr'),
                'id'          => $prefix . 'benefits_banner2_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'extramancombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 350)
            )
        );

        /**
        * Testimonials
        */
        $cmb_testimonials = new_cmb2_box(
            array(
                'id'            => '_extraman_frontpage_testimonials_id',
                'title'         => __('Testimonials', 'extramancombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Front_page',
            )
        );

        //Title
        $cmb_testimonials->add_field(
            array(
                'name'       => __('Title', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'testimonials_title',
                'type'       => 'text',
            )
        );

        //Subtitle
        $cmb_testimonials->add_field(
            array(
                'name'       => __('Subtitle', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'testimonials_subtitle',
                'type'       => 'text',
            )
        );

        //Video 1 URL
        $cmb_testimonials->add_field( 
            array(
                'name'       => __('Video 1 (URL)', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'testimonials_video1_url',
                'type'       => 'text',
            )
        );

        //Video 2 URL
        $cmb_testimonials->add_field( 
            array(
                'name'       => __('Video 2 (URL)', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'testimonials_video2_url',
                'type'       => 'text',
            )
        );

        //Video 3 URL
        $cmb_testimonials->add_field( 
            array(
                'name'       => __('Video 3 (URL)', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'testimonials_video3_url',
                'type'       => 'text',
            )
        );

        /**
        * Samples
        */
        $cmb_samples = new_cmb2_box(
            array(
                'id'            => '_extraman_frontpage_samples_id',
                'title'         => __('Samples', 'extramancombr'),
                'object_types'  => array('page'), // post type
                'show_on_cb'    => 'Show_On_Front_page',
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
            )
        );

        //Background Color
        $cmb_samples->add_field(
            array(
                'name'       => __('Background Color', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'samples_bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#fbbf24', '#f97316', '#00E6FF', '#002A54', '#001334'),
                        ),
                    ),
                ),
            ) 
        );

        //Title
        $cmb_samples->add_field(
            array(
                'name'       => __('Title', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'samples_title',
                'type'       => 'text',
            )
        );

        //Subtitle
        $cmb_samples->add_field(
            array(
                'name'       => __('Subtitle', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'samples_subtitle',
                'type'       => 'text',
            )
        );

        //Content
        $cmb_samples->add_field(
            array(
                'name'       => __('Content', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'samples_content',
                'type'       => 'textarea_code',
            )
        );

        //Button Text
        $cmb_samples->add_field(
            array(
                'name'       => __('Button Text', 'extramancombr'),
                'desc'       => '',
                'default'    => __('Buy now', 'extramancombr'),
                'id'         => $prefix . 'samples_btn_text',
                'type'       => 'text_medium',
            )
        );

        //Button URL
        $cmb_samples->add_field(
            array(
                'name'       => __('Button URL', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'samples_btn_url',
                'type'       => 'textarea_code',
                'attributes' => array (
                    'rows'  => 2,
                ),
            )
        );

        /**
        * Info Section 3
        */
        $cmb_infosec3 = new_cmb2_box(
            array(
                'id'            => '_extraman_frontpage_infosec3_id',
                'title'         => __('Info Section 3', 'extramancombr'),
                'object_types'  => array('page'), // post type
                'show_on_cb'    => 'Show_On_Front_page',
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
            )
        );

        //Background Color
        $cmb_infosec3->add_field(
            array(
                'name'       => __('Background Color', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'infosec3_bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#fbbf24', '#f97316', '#00E6FF', '#002A54', '#001334'),
                        ),
                    ),
                ),
            ) 
        );

        //Background Image (Desktop)
        $cmb_infosec3->add_field(
            array(
                'name'        => __('Background Image (Desktop)', 'extramancombr'),
                'desc'        => __('Format: JPG (recommended), PNG | Recommended size: 2560x1080px (Content centered at 1920px)', 'extramancombr'),
                'id'          => $prefix . 'infosec3_bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'extramancombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 77)
            )
        );

        //Background Image (Mobile)
        $cmb_infosec3->add_field(
            array(
                'name'        => __('Background Image (Mobile)', 'extramancombr'),
                'desc'        => __('Format: JPG (recommended), PNG | Recommended size: 1024x1280px', 'extramancombr'),
                'id'          => $prefix . 'infosec3_bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'extramancombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 350)
            )
        );

        //Title
        $cmb_infosec3->add_field(
            array(
                'name'       => __('Title', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'infosec3_title',
                'type'       => 'textarea_code',
            )
        );

        //Subtitle
        $cmb_infosec3->add_field(
            array(
                'name'       => __('Subtitle', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'infosec3_subtitle',
                'type'       => 'textarea_code',
            )
        );

        //Content
        $cmb_infosec3->add_field(
            array(
                'name'       => __('Content', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'infosec3_content',
                'type'       => 'textarea_code',
            )
        );
        
        /**
         * Hero 2
         */
        $cmb_hero2 = new_cmb2_box(
            array(
                'id'            => '_extramancombr_frontpage_hero2_id',
                'title'         => __('Hero 2', 'extramancombr'),
                'show_names'    => true,
                'object_types'  => array('page'), // post type
                'show_on_cb'    => 'Show_On_Front_page',
                'context'       => 'normal',
                'priority'      => 'high',
            )
        );

        //Background Color
        $cmb_hero2->add_field(
            array(
                'name'       => __('Background Color', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'hero2_bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#fbbf24', '#f97316', '#00E6FF', '#002A54', '#001334'),
                        ),
                    ),
                ),
            ) 
        );

        //Hero Background Image (Desktop)
        $cmb_hero2->add_field(
            array(
                'name'        => __('Background Image (Desktop)', 'extramancombr'),
                'desc'        => __('Format: JPG (recommended), PNG | Recommended size: 2560x700px (Content centered at 1920px)', 'extramancombr'),
                'id'          => $prefix . 'hero2_bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'extramancombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 77)
            )
        );

        //Hero 2 Background Image (Mobile)
        $cmb_hero2->add_field(
            array(
                'name'        => __('Background Image (Mobile)', 'extramancombr'),
                'desc'        => __('Format: JPG (recommended), PNG | Recommended size: 1024x1280px', 'extramancombr'),
                'id'          => $prefix . 'hero2_bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'extramancombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 350)
            )
        );
        
        //Hero Button Text
        $cmb_hero2->add_field(
            array(
                'name'       => __('Button Text', 'extramancombr'),
                'desc'       => '',
                'default'    => __('Buy now', 'extramancombr'),
                'id'         => $prefix . 'hero2_btn_text',
                'type'       => 'text_medium',
            )
        );

        //Hero Button URL
        $cmb_hero2->add_field(
            array(
                'name'       => __('Button URL', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'hero2_btn_url',
                'type'       => 'textarea_code',
                'attributes' => array (
                    'rows'  => 2,
                ),
            )
        );

        /**
        * FAQ
        */
        $cmb_faq = new_cmb2_box(
            array(
                'id'            => '_extraman_frontpage_faq_id',
                'title'         => __('Frequently Asked Questions', 'extramancombr'),
                'object_types'  => array('page'), // post type
                'show_on_cb'    => 'Show_On_Front_page',
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
            )
        );

        //Background Color
        $cmb_faq->add_field(
            array(
                'name'       => __('Background Color', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'faq_bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#fbbf24', '#f97316', '#00E6FF', '#002A54', '#001334'),
                        ),
                    ),
                ),
            ) 
        );

        //Title
        $cmb_faq->add_field(
            array(
                'name'       => __('Title', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'faq_title',
                'type'       => 'text',
            )
        );

        //Subtitle
        $cmb_faq->add_field(
            array(
                'name'       => __('Subtitle', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'faq_subtitle',
                'type'       => 'text',
            )
        );

        //FAQ Group
        $faq_id = $cmb_faq->add_field(
            array(
                'id'          => $prefix . 'faq_list',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   => __('FAQ {#}', 'extramancombr'),
                    'add_button'    => __('Add Another FAQ', 'extramancombr'),
                    'remove_button' => __('Remove FAQ', 'extramancombr'),
                    'sortable'      => true,
                ),
            )
        );

        //Question
        $cmb_faq->add_group_field(
            $faq_id,
            array(
                'name'       => __('Question', 'extramancombr'),
                'desc'       => '',
                'id'         => 'question',
                'type'       => 'textarea_small',
                'attributes' => array (
                    'rows'  => 2,
                ),
            )
        );

        //Answer
        $cmb_faq->add_group_field(
            $faq_id,
            array(
                'name'       => __('Answer', 'extramancombr'),
                'desc'       => '',
                'id'         => 'answer',
                'type'       => 'textarea_small',
                'attributes' => array (
                    'rows'  => 5,
                ), 
            )
        );

        /**
        * Opening hours
        */
        $cmb_opening = new_cmb2_box(
            array(
                'id'            => '_extraman_frontpage_opening_id',
                'title'         => __('Opening hours', 'extramancombr'),
                'object_types'  => array('page'), // Post type
                'show_on_cb'    => 'Show_On_Front_page',
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
            )
        );

        //Opening hours title
        $cmb_opening->add_field(
            array(
                'name'       => __('Title', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'opening_title',
                'type'       => 'text',
            )
        );

        //Opening hours
        $cmb_opening->add_field(
            array(
                'name'       => __('Content', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'opening_content',
                'type'       => 'textarea_code',
            )
        );

        //Button Text
        $cmb_opening->add_field(
            array(
                'name'       => __('Button Text', 'extramancombr'),
                'desc'       => '',
                'default'    => __('Support', 'extramancombr'),
                'id'         => $prefix . 'opening_btn_text',
                'type'       => 'text_medium',
            )
        );

        //Button URL
        $cmb_opening->add_field(
            array(
                'name'       => __('Button URL', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'opening_btn_url',
                'type'       => 'textarea_code',
                'attributes' => array (
                    'rows'  => 2,
                ),
            )
        );

        /**
        * Copyright
        */
        $cmb_copyright = new_cmb2_box(
            array(
                'id'            => '_extraman_frontpage_copyright_id',
                'title'         => __('Copyright', 'extramancombr'),
                'object_types'  => array('page'), // post type
                'show_on_cb'    => 'Show_On_Front_page',
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
            )
        );

        //Content
        $cmb_copyright->add_field(
            array(
                'name'       => __('Content', 'extramancombr'),
                'desc'       => '',
                'id'         => $prefix . 'copyright_content',
                'type'       => 'textarea_code',
            )
        );
    }
);
