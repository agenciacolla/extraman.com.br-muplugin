��    5      �  G   l      �     �     �     �     �  	   �     �     �     �               1     C     V     h     v     �     �     �  
   �     �     �  	   �     �  >   �  >     =   P  [   �  Z   �  N   E     �     �     �     �     �     �     �     �     �  
             %  	   -     7     @     I     Q     ^     d     j     v     �     �  f  �     
  !   !
     C
     W
     h
     y
     �
     �
     �
     �
     �
     �
     �
          !     7     F     R     b     w  	   �  	   �     �  B   �  B   �  A   5  e   w  d   �  S   B     �     �     �     �     �     �          "     +     >     Y     f     o     ~  
   �     �     �     �     �     �     �     �     �                         -       +       "           	   &              
                                              .   (                     %         $   1   )   3                 4       5            #   !      '   *         0   /      ,   2           Add Another Benefit Add Another FAQ Add Another Seal Add Icon Add Image Answer Background Color Background Image (Desktop) Background Image (Mobile) Banner 1 (Desktop) Banner 1 (Mobile) Banner 2 (Desktop) Banner 2 (Mobile) Benefit Image Benefit Title Benefit {#} Benefits Button Text Button URL Buy now Content Copyright FAQ {#} Format: JPG (recommended), PNG | Recommended size: 1024x1280px Format: JPG (recommended), PNG | Recommended size: 1080x1080px Format: JPG (recommended), PNG | Recommended size: 1920x450px Format: JPG (recommended), PNG | Recommended size: 2560x1080px (Content centered at 1920px) Format: JPG (recommended), PNG | Recommended size: 2560x700px (Content centered at 1920px) Format: SVG (recommended), PNG | Recommended size: 320x320 | Aspect ratio: 1:1 Frequently Asked Questions Hero Hero 2 Info Section 1 Info Section 2 Info Section 3 Opening hours Question Remove Benefit Remove FAQ Remove Seal Samples Seal Icon Seal {#} Subtitle Support Testimonials Title Video Video (URL) Video 1 (URL) Video 2 (URL) Video 3 (URL) Project-Id-Version: 
PO-Revision-Date: 2022-12-01 05:01-0300
Last-Translator: 
Language-Team: 
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.2.1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __
X-Poedit-SearchPath-0: extramancombr.php
 Adicionar Novo Benefício Adicionar Nova Pergunta Frequente Adicionar Novo Selo Adicionar ícone Adicionar imagem Resposta Cor de Fundo Imagem de fundo (Desktop) Imagem de fundo (Móvel) Banner 1 (Desktop) Banner 1 (Móvel) Banner 2 (Desktop) Banner 2 (Móvel) Imagem do Benefício Título do Benefício Benefício {#} Benefícios Texto do Botão Link do Botão (URL) Compre Agora Conteúdo Copyright Pergunta Frequente {#} Formato: JPG (recomendado), PNG | Tamanho recomendado: 1024x1280px Formato: JPG (recomendado), PNG | Tamanho recomendado: 1080x1080px Formato: JPG (recomendado), PNG | Tamanho recomendado: 1920x450px Formato: JPG (recomendado), PNG | Tamanho recomendado: 2560x1080px (Conteúdo centralizado em 1920px) Formato: JPG (recomendado), PNG | Tamanho recomendado: 2560x700px (Conteúdo centralizado em 1920px) Formato: SVG (recomendado), PNG | Tamanho recomendado: 320x320px | Proporção: 1:1 Perguntas Frequentes Hero Hero 2 Seção de Informações 1 Seção de Informações 2 Seção de Informações 3 Horário de funcionamento Pergunta Remover Benefício Remover Pergunta Frequente Remover Selo Amostras Ícone do Selo Selo {#} Subtítulo Suporte Depoimentos Título Vídeo Vídeo (URL) Vídeo 1(URL) Vídeo 2 (URL) Vídeo 3 (URL) 